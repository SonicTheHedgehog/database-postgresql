from model import Model
from view import View
from datetime import datetime
from time import time
import os
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import csv
import psycopg2

class Controller:

    def __init__(self):
        self.view = View()
        self.model = Model()
        self.mainMenu()

    # Main Menu Check

    def mainMenu(self):
        try:
            while True:
                self.view.MainMenu()
                number = self.view.selectedNumber()
                if number.isdigit():
                    switcher = {
                        1: self.customersMenu,
                        2: self.instrumentsMenu,
                        3: self.itemsMenu,
                        4: self.ordersMenu,
                        5: self.search1,
                        6: self.search2,
                        7: self.search3,
                        8: self.analyze,
                        9: self.backrest,
                        10: self.importexport,
                        11: self.chosePort,
                        12: self.exit
                    }
                    switcher.get(int(number), lambda: print("Incorrect input number"))()
                if int(number) == 12:
                    break
        except psycopg2.OperationalError:
            self.view.Error1()
        except psycopg2.errors.ReadOnlySqlTransaction:
            self.view.Error2()

    def customersMenu(self):
        try:
            self.view.customerMenu()
            number=self.view.selectedNumber()
            if number.isdigit():
                switcher={
                    1: self.CustomerOrderDisplay,
                    2: self.CustomerOrderGet,
                    3: self.CustomerOrderCreate,
                    4: self.CustomerOrderModify,
                    5: self.CustomerOrderRemove,
                    6: self.CustomerOrderRandomize,
                    7: self.mainMenu
                }
                switcher.get(int(number), lambda: print("Incorrect input number"))()
        except psycopg2.OperationalError:
            self.view.Error1()
        except psycopg2.errors.ReadOnlySqlTransaction:
            self.view.Error2()

    def instrumentsMenu(self):
        try:
            self.view.instrumentMenu()
            number=self.view.selectedNumber()
            if number.isdigit():
                switcher={
                    1: self.InstrumentDisplay,
                    2: self.InstrumentGet,
                    3: self.InstrumentCreate,
                    4: self.InstrumentModify,
                    5: self.InstrumentRemove,
                    6: self.InstrumentRandomize,
                    7: self.mainMenu
                }
                switcher.get(int(number), lambda: print("Incorrect input number"))()
        except psycopg2.OperationalError:
            self.view.Error1()
        except psycopg2.errors.ReadOnlySqlTransaction:
            self.view.Error2()

    def itemsMenu(self):
        try:
            self.view.itemMenu()
            number=self.view.selectedNumber()
            if number.isdigit():
                switcher={
                    1: self.ItemDisplay,
                    2: self.ItemGet,
                    3: self.ItemCreate,
                    4: self.ItemModify,
                    5: self.ItemRemove,
                    6: self.ItemRandomize,
                    7: self.mainMenu
                }
                switcher.get(int(number), lambda: print("Incorrect input number"))()
        except psycopg2.OperationalError:
            self.view.Error1()
        except psycopg2.errors.ReadOnlySqlTransaction:
            self.view.Error2()

    def ordersMenu(self):
        try:
            self.view.orderMenu()
            number=self.view.selectedNumber()
            if number.isdigit():
                switcher={
                    1: self.OrderDisplay,
                    2: self.OrderGet,
                    3: self.OrderCreate,
                    4: self.OrderModify,
                    5: self.OrderRemove,
                    6: self.OrderRandomize,
                    7: self.mainMenu
                }
                switcher.get(int(number), lambda: print("Incorrect input number"))()
        except psycopg2.OperationalError:
            self.view.Error1()
        except psycopg2.errors.ReadOnlySqlTransaction:
            self.view.Error2()

    def search1(self):
        try:
            name = self.view.inputName()
            if int(len(name)) > 0:
                start_time = time()
                self.view.printSearch1(self.model.getCustomerOrders(name))
                end_time = time()
                self.view.printDefTime(start_time, end_time)
            else:
                self.view.newError()
        except psycopg2.OperationalError:
            self.view.Error1()
        except psycopg2.errors.ReadOnlySqlTransaction:
            self.view.Error2()

    def search2(self):
        try:
            country = self.view.inputCountry()
            category=self.view.inputCategory()
            if int(len(country)) > 0 or int(len(category)) > 0:
                start_time = time()
                self.view.printSearch2(self.model.getInstrumentByCountryOrCategoty(country, category))
                end_time = time()
                self.view.printDefTime(start_time, end_time)
            else: self.view.newError()
        except psycopg2.OperationalError:
            self.view.Error1()
        except psycopg2.errors.ReadOnlySqlTransaction:
            self.view.Error2()

    def search3(self):
        try:
            self.view.analyzemenu()
            number = self.view.selectedNumber()
            if number.isdigit():
                switcher = {
                    1: self.analyzeRating,
                    2: self.getGraph,
                    3: self.mainMenu
                }
                switcher.get(int(number), lambda: print("Incorrect input number"))()
        except psycopg2.OperationalError:
            self.view.Error1()
        except psycopg2.errors.ReadOnlySqlTransaction:
            self.view.Error2()

    def importexport(self):
        try:
            self.view.importexportmenu()
            number = self.view.selectedNumber()
            if number.isdigit():
                switcher = {
                    1: self.importData,
                    2: self.exportData,
                    3: self.mainMenu
                }
                switcher.get(int(number), lambda: print("Incorrect input number"))()
        except psycopg2.OperationalError:
            self.view.Error1()
        except psycopg2.errors.ReadOnlySqlTransaction:
            self.view.Error2()

    def analyzeRating(self):
        try:
            low = self.view.inputLowRating()
            high = self.view.inputHighRating()
            if low.isdigit() and high.isdigit():
                if low<high:
                    start_time = time()
                    self.view.printSearch3(self.model.getItemByRating(low, high))
                    end_time = time()
                    self.view.printDefTime(start_time, end_time)
                else:
                    self.view.newError()
            else:
                self.view.newError()
        except psycopg2.OperationalError:
                self.view.Error1()
        except psycopg2.errors.ReadOnlySqlTransaction:
                self.view.Error2()


    def backrest(self):
        try:
            self.view.backrestmenu()
            number = self.view.selectedNumber()
            if number.isdigit():
                switcher = {
                    1: self.backup,
                    2: self.restore,
                    3: self.mainMenu
                }
                switcher.get(int(number), lambda: print("Incorrect input number"))()
        except psycopg2.OperationalError:
                self.view.Error1()
        except psycopg2.errors.ReadOnlySqlTransaction:
                self.view.Error2()

    def getGraph(self):
        try:
            items = self.model.getTable2()
            names = []
            rating = []
            for item in items:
                names.append(item[0])
                rating.append(item[1])
            x = np.arange(len(items))
            width = 0.75
            fig, ax = plt.subplots()
            rects = ax.plot(x, rating, width)
            ax.set_title('The rating of musical instruments')
            ax.set_xticks(x)
            ax.set_ylabel('Rating')
            ax.set_xticklabels(names)
            fig.tight_layout()
            fig.set_size_inches(20, 10)
            plt.show()
        except psycopg2.OperationalError:
                self.view.Error1()
        except psycopg2.errors.ReadOnlySqlTransaction:
                self.view.Error2()

    def analyze(self):
        try:
            quantity=self.view.inputQuantity()
            if quantity=="top":
                self.view.printAnalysation(self.model.getItemByQuantitySold())
                items=self.model.getTable()
                names=[]
                counts=[]
                for item in items:
                    names.append(item[0])
                    counts.append(item[1])
                x=np.arange(len(items))
                width=0.75
                fig, ax=plt.subplots()
                rects= ax.bar(x, counts, width)
                ax.set_ylabel('Quantity sold')
                ax.set_title('Top 10 sold instruments')
                ax.set_xticks(x)
                ax.set_xticklabels(names)
                for rect in rects:
                    height=rect.get_height()
                    ax.annotate('{}'.format(height),
                                xy=(rect.get_x()+rect.get_width()/2,height),
                                xytext=(0, 0),
                                textcoords="offset points",
                                ha='center' , va='bottom')
                fig.tight_layout()
                fig.set_size_inches(20,10)
                plt.show()
            elif quantity=="max":
                self.view.printAnalysation(self.model.getMAXItemByQuantitySold())
            elif quantity=="min":
                self.view.printAnalysation(self.model.getMINItemByQuantitySold())
            else: self.view.newError()
        except psycopg2.OperationalError:
                self.view.Error1()
        except psycopg2.errors.ReadOnlySqlTransaction:
                self.view.Error2()

    def backup(self):
        try:
            filename = self.view.inputFilename()
            os.system(f'/usr/bin/pg_dump --file "/home/sonya/DB/bd/coursework/backups/{filename}.sql" --host "localhost" --port "5432" --username "postgres" --password --verbose --format=c --blobs "music"')
        except psycopg2.OperationalError:
                self.view.Error1()
        except psycopg2.errors.ReadOnlySqlTransaction:
                self.view.Error2()

    def restore(self):
        try:
            filepath='/home/sonya/DB/bd/coursework/backups/'
            filename = self.view.inputFilename()
            os.system(f'/usr/bin/pg_restore -d tmp -U sonya -c {filepath+filename}')
        except psycopg2.OperationalError:
                self.view.Error1()
        except psycopg2.errors.ReadOnlySqlTransaction:
                self.view.Error2()

    def importData(self):
        try:
            filepath = '/home/sonya/DB/bd/coursework/backups/'
            filename = self.view.inputFilename()
            file = filepath + filename
            with open(file , 'r') as csvfile:
                reader = csv.DictReader(csvfile)
                for row in reader:
                    self.model.addInstrument(row['name'], row['country'], row['category'])
        except psycopg2.OperationalError:
                self.view.Error1()
        except psycopg2.errors.ReadOnlySqlTransaction:
                self.view.Error2()

    def exportData(self):
        try:
            filepath = '/home/sonya/DB/bd/coursework/backups/'
            filename = self.view.inputFilename()
            file = filepath + filename
            with open(file, 'w', newline='') as csvfile:
                fieldnames = ['id', 'name', 'country', 'category']
                writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
                writer.writeheader()
                instruments = self.model.getInstrument()
                for instrument in instruments:
                    writer.writerow({'id': instrument[0], 'name': instrument[1], 'country': instrument[2], 'category': instrument[3]})
        except psycopg2.OperationalError:
                self.view.Error1()
        except psycopg2.errors.ReadOnlySqlTransaction:
                self.view.Error2()

    def chosePort(self):
        try:
            port=self.view.inputPort()
            self.model.chosenPort(port)
        except psycopg2.OperationalError:
                self.view.Error1()
        except psycopg2.errors.ReadOnlySqlTransaction:
                self.view.Error2()

    #Customer order
    def CustomerOrderDisplay(self):
        try:
            self.view.printCustomerOrder(self.model.getCustomerOrder())
        except psycopg2.OperationalError:
                self.view.Error1()
        except psycopg2.errors.ReadOnlySqlTransaction:
                self.view.Error2()

    def CustomerOrderGet(self):
        try:
            id_=self.view.inputId()
            if id_.isdigit():
                if self.model.getCustId(id_)[0]>0:
                    print(f'{self.model.getCustId(id_)[0]}')
                    main=self.model.getCustomerOrderbyId(int(id_))
                    self.view.printCustomerOrder(main)
                else: self.view.noId()
            else: self.view.noNum("id")
        except psycopg2.OperationalError:
                self.view.Error1()
        except psycopg2.errors.ReadOnlySqlTransaction:
                self.view.Error2()

    def CustomerOrderCreate(self):
        try:
            name=self.view.inputName()
            status=self.view.inputStatus()
            time=self.view.inputTime()
            if datetime.strptime(time, '%Y-%m-%d %H:%M:%S') and (status or not status) and int(len(name))>0:
                self.model.addCustomerOrder(name, status, time)
            else: self.view.newError()
        except psycopg2.OperationalError:
                self.view.Error1()
        except psycopg2.errors.ReadOnlySqlTransaction:
                self.view.Error2()

    def CustomerOrderModify(self):
        try:
            id_=self.view.inputId()
            name = self.view.inputName()
            status = self.view.inputStatus()
            time = self.view.inputTime()
            if datetime.strptime(time, '%Y-%m-%d %H:%M:%S') and (status or not status) and int(len(name)) > 0:
                if self.model.getCustId(id_)[0]>0:
                    self.model.updateCustomerOrder(id_, name, status, time)
                    row=self.model.getCustomerOrderbyId(id_)
                    self.view.printCustomerOrder(row)
                else:
                    self.view.noId()
            else: self.view.newError()
        except psycopg2.OperationalError:
                self.view.Error1()
        except psycopg2.errors.ReadOnlySqlTransaction:
                self.view.Error2()

    def CustomerOrderRemove(self):
        try:
            id_=self.view.inputId()
            if self.model.getCustId(id_)[0]>0:
                row = self.model.getCustomerOrderbyId(id_)
                self.view.printCustomerOrder(row)
                self.model.getOrderbyCustomer(id_)
                self.model.deleteOrderbyCustomer(id_)
                self.model.deleteCustomerOrder(id_)
            else:
                self.view.noId()
        except psycopg2.OperationalError:
                self.view.Error1()
        except psycopg2.errors.ReadOnlySqlTransaction:
                self.view.Error2()

    def CustomerOrderRandomize(self):
        try:
            quantity=self.view.inputQuantityin()
            if quantity.isnumeric():
                self.model.generateCustomerOrder(quantity)
                self.CustomerOrderDisplay()
            else:
                self.view.noNum(quantity)
        except psycopg2.OperationalError:
                self.view.Error1()
        except psycopg2.errors.ReadOnlySqlTransaction:
                self.view.Error2()

#instrument
    def InstrumentDisplay(self):
        try:
            self.view.printInstrument(self.model.getInstrument())
        except psycopg2.OperationalError:
                self.view.Error1()
        except psycopg2.errors.ReadOnlySqlTransaction:
                self.view.Error2()

    def InstrumentGet(self):
        try:
            id_=self.view.inputId()
            if id_.isdigit():
                if self.model.getInstId(id_)[0]>0:
                    self.view.printInstrument(self.model.getInstrumentbyId(int(id_)))
                else:
                    self.view.noId()
            else: self.view.noNum("id")
        except psycopg2.OperationalError:
                self.view.Error1()
        except psycopg2.errors.ReadOnlySqlTransaction:
                self.view.Error2()

    def InstrumentCreate(self):
        try:
            name=self.view.inputName()
            country=self.view.inputCountry()
            category=self.view.inputCategory()
            if int(len(name))>0 and int(len(country))>0 and int(len(category))>0 :
                self.model.addInstrument(name, country, category)
            else: self.view.newError()
        except psycopg2.OperationalError:
                self.view.Error1()
        except psycopg2.errors.ReadOnlySqlTransaction:
                self.view.Error2()

    def InstrumentModify(self):
        try:
            id_=self.view.inputId()
            name = self.view.inputName()
            country = self.view.inputCountry()
            category = self.view.inputCategory()
            if int(len(name))>0 and int(len(country))>0 and int(len(category))>0:
                if self.model.getInstId(id_)[0]>0:
                    self.model.getInstrumentbyId(id_)
                    self.model.updateInstrument(id_, name, country, category)
                    row=self.model.getInstrumentbyId(id_)
                    self.view.printInstrument(row)
                else:
                    self.view.noId()
            else:
                self.view.newError()
        except psycopg2.OperationalError:
                self.view.Error1()
        except psycopg2.errors.ReadOnlySqlTransaction:
                self.view.Error2()

    def InstrumentRemove(self):
        try:
            id_=self.view.inputId()
            if self.model.getInstId(id_)[0]>0:
                row = self.model.getInstrumentbyId(id_)
                self.view.printInstrument(row)
                self.model.getItembyInstrumentId(id_)
                self.model.deleteItembyInstrumentId(id_)
                self.model.deleteInstrument(id_)
            else:
                self.view.noId()
        except psycopg2.OperationalError:
                self.view.Error1()
        except psycopg2.errors.ReadOnlySqlTransaction:
                self.view.Error2()

    def InstrumentRandomize(self):
        try:
            quantity=self.view.inputQuantityin()
            if quantity.isnumeric():
                self.model.generateInstruments(quantity)
                self.InstrumentDisplay()
            else:
                self.view.noNum(quantity)
        except psycopg2.OperationalError:
                self.view.Error1()
        except psycopg2.errors.ReadOnlySqlTransaction:
                self.view.Error2()

#item
    def ItemDisplay(self):
        try:
            self.view.printItem(self.model.getItem())
        except psycopg2.OperationalError:
                self.view.Error1()
        except psycopg2.errors.ReadOnlySqlTransaction:
                self.view.Error2()

    def ItemGet(self):
        try:
            id_=self.view.inputId()
            if id_.isdigit():
                if self.model.getItemId(id_)[0]>0:
                    self.view.printItem(self.model.getItembyId(int(id_)))
                else:
                    self.view.noId()
            else:
                self.view.noNum("id")
        except psycopg2.OperationalError:
                self.view.Error1()
        except psycopg2.errors.ReadOnlySqlTransaction:
                self.view.Error2()

    def ItemCreate(self):
        try:
            instrument_id=self.view.inputIdofInstrument()
            quantity_sold=self.view.inputYear()
            description=self.view.inputDescription()
            if instrument_id.isdigit() and quantity_sold.isdigit() and 0 < int(quantity_sold) < 1000 and int(len(description))>0 :
                if self.model.getInstId(instrument_id)[0]>0:
                    self.model.getInstrumentbyId(instrument_id)
                    self.model.addItem(instrument_id, quantity_sold, description)
                else:
                    self.view.noId()
            else: self.view.newError()
        except psycopg2.OperationalError:
                self.view.Error1()
        except psycopg2.errors.ReadOnlySqlTransaction:
                self.view.Error2()

    def ItemModify(self):
        try:
            id_=self.view.inputId()
            instrument_id = self.view.inputIdofInstrument()
            quantity_sold = self.view.inputYear()
            description = self.view.inputDescription()
            if instrument_id.isdigit() and quantity_sold.isdigit() and 0 < int(quantity_sold) < 1000 and int(len(description))>0 :
                if self.model.getItemId(id_)[0]>0 and self.model.getInstId(instrument_id)[0]>0:
                    self.model.getItembyInstrumentId(instrument_id)
                    self.model.updateItem(id_, instrument_id, quantity_sold, description)
                    row=self.model.getItembyId(id_)
                    self.view.printItem(row)
                else:
                    self.view.noId()
            else:
                self.view.newError()
        except psycopg2.OperationalError:
                self.view.Error1()
        except psycopg2.errors.ReadOnlySqlTransaction:
                self.view.Error2()

    def ItemRemove(self):
        try:
            id_=self.view.inputId()
            if self.model.getItemId(id_)[0]>0:
                row = self.model.getItembyId(id_)
                self.view.printItem(row)
                self.model.getOrderbyItem(id_)
                self.model.deleteOrderbyItem(id_)
                self.model.deleteItem(id_)
            else:
                self.view.noId()
        except psycopg2.OperationalError:
                self.view.Error1()
        except psycopg2.errors.ReadOnlySqlTransaction:
                self.view.Error2()

    def ItemRandomize(self):
        try:
            quantity=self.view.inputQuantityin()
            if quantity.isnumeric():
                self.model.generateItems(quantity)
                self.ItemDisplay()
            else:
                self.view.noNum(quantity)
        except psycopg2.OperationalError:
                self.view.Error1()
        except psycopg2.errors.ReadOnlySqlTransaction:
                self.view.Error2()

#order
    def OrderDisplay(self):
        try:
            self.view.printOrder(self.model.getOrder())
        except psycopg2.OperationalError:
                self.view.Error1()
        except psycopg2.errors.ReadOnlySqlTransaction:
                self.view.Error2()

    def OrderGet(self):
        try:
            id_=self.view.inputId()
            if id_.isdigit():
                if self.model.getOrderId(id_)[0]> 0:
                    self.view.printOrder(self.model.getOrderbyId(int(id_)))
                else:
                    self.view.noId()
            else:
                self.view.noNum("id")
        except psycopg2.OperationalError:
                self.view.Error1()
        except psycopg2.errors.ReadOnlySqlTransaction:
                self.view.Error2()

    def OrderCreate(self):
        try:
            customer_order_id=self.view.inputIdOfOrder()
            item_id=self.view.inputIdOfItem()
            rating=self.view.inputRating()
            if customer_order_id.isdigit() and item_id.isdigit() and rating.isdigit() and int(rating)>0:
                if self.model.getCustId(customer_order_id)[0]> 0 and self.model.getItemId(item_id)[0]> 0:
                    self.model.getItembyId(item_id)
                    self.model.getCustomerOrderbyId(customer_order_id)
                    self.model.addOrder(customer_order_id, item_id, rating)
                else:
                    self.view.noId()
            else:
                self.view.newError()
        except psycopg2.OperationalError:
                self.view.Error1()
        except psycopg2.errors.ReadOnlySqlTransaction:
                self.view.Error2()

    def OrderModify(self):
        try:
            id_=self.view.inputId()
            customer_order_id = self.view.inputIdOfOrder()
            item_id = self.view.inputIdOfItem()
            rating = self.view.inputRating()
            if customer_order_id.isdigit() and item_id.isdigit() and rating.isdigit() and int(rating) > 0:
                if self.model.getCustId(customer_order_id)[0]> 0 and self.model.getItemId(item_id)[0]> 0 and self.model.getOrderId(id_)[0]>0:
                    self.model.getItembyId(item_id)
                    self.model.getCustomerOrderbyId(customer_order_id)
                    self.model.updateOrder(id_, customer_order_id, item_id, rating)
                    row=self.model.getOrderbyId(id_)
                    self.view.printOrder(row)
                else:
                    self.view.noId()
            else:
                self.view.newError()
        except psycopg2.OperationalError:
                self.view.Error1()
        except psycopg2.errors.ReadOnlySqlTransaction:
                self.view.Error2()

    def OrderRemove(self):
        try:
            id_=self.view.inputId()
            if  self.model.getOrderId(id_)[0]> 0:
                row = self.model.getOrderbyId(id_)
                self.view.printOrder(row)
                self.model.deleteOrder(id_)
            else:
                self.view.noId()
        except psycopg2.OperationalError:
                self.view.Error1()
        except psycopg2.errors.ReadOnlySqlTransaction:
                self.view.Error2()

    def OrderRandomize(self):
        try:
            quantity=self.view.inputQuantityin()
            if quantity.isnumeric():
                self.model.generateOrderItem(quantity)
                self.OrderDisplay()
            else: self.view.noNum(quantity)
        except psycopg2.OperationalError:
                self.view.Error1()
        except psycopg2.errors.ReadOnlySqlTransaction:
                self.view.Error2()

    def exit(self):
        try:
            self.view.printExit()
        except psycopg2.OperationalError:
                self.view.Error1()
        except psycopg2.errors.ReadOnlySqlTransaction:
                self.view.Error2()