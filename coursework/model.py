import psycopg2
import string
from datetime import date
import random
from sqlalchemy import create_engine, update, delete
from sqlalchemy.orm import sessionmaker

class Model(object):
    def __init__(self, dbname_='music', user_='postgres', password_='1234576', host_='localhost', port='5432'):
        self.con = psycopg2.connect(dbname=f'{dbname_}', user=f'{user_}', password=f'{password_}', host=f'{host_}', port=f'{port}')
        self.cursor = self.con.cursor()

    def __del__(self):
        self.con.close()

    def chosenPort(self, port='5432'):
        self.con.close()
        self.cursor.close()
        self.con = psycopg2.connect(dbname='music', user='postgres', password='1234576', host= 'localhost',port=f'{port}')
        self.cursor = self.con.cursor()

    def getInstId(self, id):
        self.cursor.execute(f'SELECT count(*) FROM instrument WHERE id={id}')
        count = self.cursor.fetchall()[0]
        return count

    def getInstrument(self):
        self.cursor.execute('SELECT * FROM instrument ORDER BY id')
        rows = self.cursor.fetchall()
        return rows

    def getInstrumentbyId(self, id):
        self.cursor.execute(f'SELECT * FROM instrument WHERE id={id}')
        item = self.cursor.fetchall()
        return item

    def addInstrument(self, name, country, category):
        self.cursor.execute(f"INSERT INTO instrument (name, country, category) VALUES ('{name}', '{country}', '{category}')")
        self.con.commit()

    def updateInstrument(self, id, name, country, category):
        self.cursor.execute(f"UPDATE instrument SET name = '{name}', country = '{country}', category = '{category}' WHERE id ={id} ")
        self.con.commit()

    def deleteInstrument(self, id):
        self.cursor.execute(f"DELETE FROM instrument WHERE id = {id}")
        self.con.commit()

    ###################
    def getCustId(self, id):
        self.cursor.execute(f'SELECT count(*) FROM customer_order WHERE id={id}')
        count = self.cursor.fetchall()[0]
        return count

    def getCustomerOrder(self):
        self.cursor.execute('SELECT * FROM customer_order ORDER BY id')
        rows = self.cursor.fetchall()
        return rows

    def getCustomerOrderbyId(self, id):
        self.cursor.execute(f'SELECT * FROM customer_order WHERE id={id}')
        item = self.cursor.fetchall()
        return item

    def addCustomerOrder(self, customer_name, status, time):
        self.cursor.execute(f"INSERT INTO customer_order (customer_name, status, time) VALUES ('{customer_name}', '{status}', '{time}')")
        self.con.commit()

    def updateCustomerOrder(self, id, customer_name, status, time):
        self.cursor.execute(f"UPDATE customer_order SET customer_name = '{customer_name}', status = '{status}', time = '{time}' WHERE id ={id} ")
        self.con.commit()

    def deleteCustomerOrder(self, id):
        self.cursor.execute(f"DELETE FROM customer_order WHERE id = {id}")
        self.con.commit()

###################
    def getItemId(self, id):
        self.cursor.execute(f'SELECT count(*) FROM item WHERE id={id}')
        count = self.cursor.fetchall()[0]
        return count

    def getItem(self):
        self.cursor.execute('SELECT * FROM item ORDER BY id')
        rows = self.cursor.fetchall()
        return rows

    def getItembyId(self, id):
        self.cursor.execute(f'SELECT * FROM item WHERE id={id}')
        item = self.cursor.fetchall()
        return item


    def addItem(self, instrument_id, quantity_sold, description):
        self.cursor.execute(f"INSERT INTO item (instrument_id, quantity_sold, description) VALUES ('{instrument_id}', '{quantity_sold}', '{description}')")
        self.con.commit()


    def updateItem(self, id, instrument_id, quantity_sold, description):
        self.cursor.execute(
            f"UPDATE item SET instrument_id = '{instrument_id}', quantity_sold = '{quantity_sold}', description = '{description}' WHERE id ={id} ")
        self.con.commit()


    def deleteItem(self, id):
        self.cursor.execute(f"DELETE FROM item WHERE id = {id}")
        self.con.commit()
    ###################
    def getOrderId(self, id):
        self.cursor.execute(f'SELECT count(*) FROM order_item WHERE id={id}')
        count = self.cursor.fetchall()[0]
        return count

    def getOrder(self):
        self.cursor.execute('SELECT * FROM order_item ORDER BY id')
        rows = self.cursor.fetchall()
        return rows


    def getOrderbyId(self, id):
        self.cursor.execute(f'SELECT * FROM order_item WHERE id={id}')
        item = self.cursor.fetchall()
        return item


    def addOrder(self, customer_order_id, item_id, rating):
        self.cursor.execute(
            f"INSERT INTO order_item (customer_order_id, item_id, rating) VALUES ('{customer_order_id}', '{item_id}', '{rating}')")
        self.con.commit()


    def updateOrder(self, id, customer_order_id, item_id, rating):
        self.cursor.execute(
            f"UPDATE order_item SET customer_order_id = '{customer_order_id}', item_id = '{item_id}', rating = '{rating}' WHERE id ={id} ")
        self.con.commit()


    def deleteOrder(self, id):
        self.cursor.execute(f"DELETE FROM order_item WHERE id = {id}")
        self.cursor.execute(f"UPDATE customer_order SET status = 'False' WHERE id={id} ")
        self.con.commit()

    def getItembyInstrumentId(self, instrument_id):
        self.cursor.execute(f'SELECT * FROM item WHERE instrument_id={instrument_id}  ORDER BY id')
        rows = self.cursor.fetchall()
        return rows

    def getOrderbyCustomer(self, customer_order_id):
        self.cursor.execute(f'SELECT * FROM order_item WHERE customer_order_id={customer_order_id}  ORDER BY id')
        rows = self.cursor.fetchall()
        return rows


    def getOrderbyItem(self, item_id):
        self.cursor.execute(f'SELECT * FROM order_item WHERE item_id={item_id} ORDER BY id')
        item = self.cursor.fetchall()
        return item

    def deleteItembyInstrumentId(self, instrument_id):
        self.cursor.execute(f"DELETE FROM item WHERE instrument_id = {instrument_id}")
        self.con.commit()

    def deleteOrderbyCustomer(self, customer_order_id):
        self.cursor.execute(f"DELETE FROM order_item WHERE customer_order_id = {customer_order_id}")
        self.con.commit()

    def deleteOrderbyItem(self, item_id):
        self.cursor.execute(f"DELETE FROM order_item WHERE item_id = {item_id}")
        self.con.commit()

    def generateInstruments(self, quantity):
        self.cursor.execute(f"""INSERT INTO instrument (name, country, category) select 
        chr(trunc(65 + random()*57)::int) || chr(trunc(65 + random()*57)::int) || chr(trunc(65 + random()*57)::int) || 
        chr(trunc(65 + random()*57)::int), chr(trunc(65 + random()*57)::int) || chr(trunc(65 + random()*57)::int) || 
        chr(trunc(65 + random()*57)::int) || chr(trunc(65 + random()*57)::int), chr(trunc(65 + random()*57)::int) || 
        chr(trunc(65 + random()*57)::int) || chr(trunc(65 + random()*57)::int) || chr(trunc(65 + random()*57)::int) 
        from generate_series(1, {quantity})""")
        self.con.commit()

    def generateItems(self,quantity):
        self.cursor.execute(f"""INSERT INTO item (instrument_id, quantity_sold, description) select 
        get_instrument_id(), floor(random() * (1000-1+1) + 1)::int,
        chr(trunc(65 + random()*57)::int) || chr(trunc(65 + random()*57)::int) || chr(trunc(65 + random()*57)::int) || 
        chr(trunc(65 + random()*57)::int)
        from generate_series(1, {quantity})""")
        self.con.commit()

    def generateCustomerOrder(self, quantity):
        self.cursor.execute(f"""INSERT INTO customer_order (customer_name, status, time) select 
        chr(trunc(65 + random()*57)::int) || chr(trunc(65 + random()*57)::int) || chr(trunc(65 + random()*57)::int) || 
        chr(trunc(65 + random()*57)::int),
        False,
        timestamp '2010-01-10 20:00:00' + random() * (timestamp '2010-01-20 20:00:00' - timestamp '2020-12-20 10:00:00')
        from generate_series(1, {quantity})""")
        self.con.commit()

    def generateOrderItem(self, quantity):
        self.cursor.execute(f"""INSERT INTO order_item (customer_order_id, item_id, rating) select 
        get_customer_order_id(),
        get_item_id(), floor(random() *(10-1+1) + 1)::int
        from generate_series(1, {quantity})""")
        self.con.commit()

    def getCustomerOrders(self, customer_name=""):
        self.cursor.execute(f"""with main as(SELECT item_id FROM order_item 
        INNER JOIN (SELECT id FROM customer_order WHERE customer_name='{customer_name}') AS c ON 
        order_item.customer_order_id=c.id)
        SELECT id, name, country, category FROM instrument
        INNER JOIN main 
        ON instrument.id=main.item_id
        ORDER BY id""")
        rows = self.cursor.fetchall()
        return rows

    def getInstrumentByCountryOrCategoty(self, country="", category=""):
        self.cursor.execute(f""" with main as (SELECT * FROM instrument 
        WHERE  category  like '%{category}%' AND country like '%{country}%')
        select main.name, main.category, main.country, item.quantity_sold, item.description
        from main inner join item 
        on item.instrument_id=main.id""")
        rows = self.cursor.fetchall()
        return rows

    def getItemByRating(self, low, high):
        self.cursor.execute(f"""with main as(select item_id, rating from order_item where rating>={low} and rating<={high})
        select name, country, main2.rating from instrument 
        inner join (select item.instrument_id, main.rating from item inner join main on item.id=main.item_id) as main2
        on instrument.id=main2.instrument_id""")
        rows = self.cursor.fetchall()
        return rows

    def getMAXItemByQuantitySold(self):
        self.cursor.execute(f"""with main as(select quantity_sold, instrument_id as id from item where 
        quantity_sold=(select max(quantity_sold) from item))
        select name, country, category, main.quantity_sold from instrument inner join main
        on instrument.id=main.id""")
        rows = self.cursor.fetchall()
        return rows

    def getItemByQuantitySold(self):
        self.cursor.execute(f"""with main as(select quantity_sold, instrument_id as id from item 
        order by quantity_sold DESC limit 10)
        select name, country, category, main.quantity_sold from instrument inner join main
        on instrument.id=main.id """)
        rows = self.cursor.fetchall()
        return rows

    def getMINItemByQuantitySold(self):
        self.cursor.execute(f"""with main as(select quantity_sold, instrument_id as id from item where 
        quantity_sold=(select min(quantity_sold) from item))
        select name, country, category, main.quantity_sold from instrument inner join main
        on instrument.id=main.id""")
        rows = self.cursor.fetchall()
        return rows

    def getTable(self):
        self.cursor.execute(f"""with main as(select quantity_sold, instrument_id as id from item 
        order by quantity_sold DESC limit 10)
        select name, main.quantity_sold from instrument inner join main
        on instrument.id=main.id """)
        rows = self.cursor.fetchall()
        return rows

    def getTable2(self):
        self.cursor.execute(f"""with main as(select item_id, rating from order_item)
        select distinct name, main2.rating from instrument 
        inner join (select item.instrument_id, main.rating from item inner join main on item.id=main.item_id) as main2
        on instrument.id=main2.instrument_id""")
        rows = self.cursor.fetchall()
        return rows