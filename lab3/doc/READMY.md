Name: Засоби оптимізації роботи СУБД PostgreSQL
Data Base:
instrument
 INT id <PK>
 TEXT name
 TEXT country
 TEXT category
item
 INT id <PK>
 INT instrument_id <FK>
 INT year_of_production
 TEXT description
customer_order
 INT id <PK>
 TEXT customer_name
 BOOL status
 TIME time
order_item
 INT id <PK>
 INT customer_order_id <FK>
 INT item_id <FK>
 INT price
 
instrument->item
item<->customer_order
