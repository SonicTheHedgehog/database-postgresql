from sqlalchemy import Column, Integer, String, Time, Boolean
from models.base import Base

class Customer(Base):
    __tablename__ = 'customer_order'

    id = Column(Integer, primary_key=True)
    customer_name = Column(String)
    status = Column(Boolean)
    time = Column(Time)

    def __init__(self, customer_name, status, time):
         self.customer_name = customer_name
         self.status = status
         self.time = time