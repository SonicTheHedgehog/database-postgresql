from sqlalchemy import Column, Integer, String
from models.base import Base

class Instrument(Base):
    __tablename__ = 'instrument'

    id = Column(Integer, primary_key=True)
    name = Column(String)
    country = Column(String)
    category = Column(String)

    def __init__(self, name, country, category):
         self.name = name
         self.country = country
         self.category = category