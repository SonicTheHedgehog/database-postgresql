from sqlalchemy import Column, Integer, String, ForeignKey
from sqlalchemy.orm import relationship
from models.base import Base
from models.Instrument import Instrument

class Item(Base):
    __tablename__ = 'item'

    id = Column(Integer, primary_key=True)
    instrument_id = Column(Integer,  ForeignKey('instrument.id'))
    year_of_production = Column(Integer)
    description = Column(String)

    def __init__(self, instrument_id,  year_of_production, description):
         self.instrument_id = instrument_id
         self.year_of_production = year_of_production
         self.description = description