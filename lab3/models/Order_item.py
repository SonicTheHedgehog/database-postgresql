from sqlalchemy import Column, Integer, String, ForeignKey
from sqlalchemy.orm import relationship
from models.base import Base
from models.Item import Item
from models.Customer_order import Customer

class Order(Base):
    __tablename__ = 'order_item'

    id = Column(Integer, primary_key=True)
    customer_order_id = Column(Integer,  ForeignKey('customer_order.id'))
    item_id = Column(Integer, ForeignKey('item.id'))
    price = Column(Integer)

    def __init__(self,  customer_order_id, item_id, price):
         self.customer_order_id = customer_order_id
         self.item_id = item_id
         self.price = price