import psycopg2
import string
from datetime import date
import random
from sqlalchemy import create_engine, update, delete
from sqlalchemy.orm import sessionmaker
from models.Instrument import Instrument
from models.Item import Item
from models.Customer_order import Customer
from models.Order_item import Order

class Model(object):
    def __init__(self, dbname_='postgres', user_='postgres', password_='12345687', host_='localhost'):
        self.con = psycopg2.connect(dbname=f'{dbname_}', user=f'{user_}', password=f'{password_}', host=f'{host_}')
        self.cursor = self.con.cursor()
        self.engine = create_engine('postgresql+psycopg2://postgres:12345687@localhost:5432/postgres')
        self.Session = sessionmaker(bind=self.engine)
        self.session = self.Session()


    def __del__(self):
        self.con.close()


    def getInstrument(self):
        instruments = []
        for instrument in self.session.query(Instrument).order_by(Instrument.id).all():
            instruments.append((instrument.id, instrument.name, instrument.country, instrument.category))
        return instruments

    def getInstrumentbyId(self, id):
        instruments = []
        for instrument in self.session.query(Instrument).order_by(Instrument.id).filter(Instrument.id==id):
            instruments.append((instrument.id, instrument.name, instrument.country, instrument.category))
        return instruments

    def addInstrument(self, name, country, category):
        self.session.add(Instrument(name, country, category))
        self.session.commit()

    def updateInstrument(self, id, name, country, category):
        self.session.execute(update(Instrument).where(Instrument.id == id).values(name = name, country = country, category = category))
        self.session.commit()

    def deleteInstrument(self, id):
        self.session.execute(delete(Instrument).where(Instrument.id == id))
        self.session.commit()

    ###################
    def getCustomerOrder(self):
        customers = []
        for customer_order in self.session.query(Customer).order_by(Customer.id).all():
            customers.append((customer_order.id, customer_order.customer_name, customer_order.status, customer_order.time))
        return customers

    def getCustomerOrderbyId(self, id):
        customers = []
        for customer_order in self.session.query(Customer).order_by(Customer.id).filter(Customer.id == id):
            customers.append((customer_order.id, customer_order.customer_name, customer_order.status, customer_order.time))
        return customers

    def addCustomerOrder(self, customer_name, status, time):
        self.session.add(Customer(customer_name, status, time))
        self.session.commit()

    def updateCustomerOrder(self, id, customer_name, status, time):
        self.session.execute(update(Customer).where(Customer.id == id).values(customer_name = customer_name, status = status, time = time))
        self.session.commit()

    def deleteCustomerOrder(self, id):
        self.session.execute(delete(Customer).where(Customer.id == id))
        self.session.commit()

###################
    def getItem(self):
        items = []
        for item in self.session.query(Item).order_by(Item.id).all():
            items.append((item.id, item.instrument_id, item.year_of_production, item.description))
        return items


    def getItembyId(self, id):
        items = []
        for item in self.session.query(Item).order_by(Item.id).filter(Item.id == id):
            items.append((item.id, item.instrument_id, item.year_of_production, item.description))
        return items


    def addItem(self, instrument_id, year_of_production, description):
        self.session.add(Item(instrument_id, year_of_production, description))
        self.session.commit()


    def updateItem(self, id, instrument_id, year_of_production, description):
        self.session.execute(update(Item).where(Item.id == id).values(instrument_id = instrument_id, year_of_production = year_of_production, description = description))
        self.session.commit()


    def deleteItem(self, id):
        self.session.execute(delete(Item).where(Item.id == id))
        self.session.commit()
    ###################

    def getOrder(self):
        orders = []
        for order in self.session.query(Order).order_by(Order.id).all():
            orders.append((order.id, order.customer_order_id, order.item_id, order.price))
        return orders


    def getOrderbyId(self, id):
        orders = []
        for order in self.session.query(Order).order_by(Order.id).filter(Order.id == id):
            orders.append((order.id, order.customer_order_id, order.item_id, order.price))
        return orders


    def addOrder(self, customer_order_id, item_id, price):
        self.session.add(Order(customer_order_id, item_id, price))
        self.session.commit()


    def updateOrder(self, id, customer_order_id, item_id, price):
        self.session.execute(update(Order).where(Order.id == id).values(customer_order_id = customer_order_id, item_id = item_id, price = price))
        self.session.commit()


    def deleteOrder(self, id):
        self.session.execute(delete(Order).where(Order.id == id))
        self.session.commit()

    def getItembyInstrumentId(self, instrument_id):
        items = []
        for item in self.session.query(Item).order_by(Item.id).filter(Item.instrument_id == instrument_id):
            items.append((item.id, item.instrument_id, item.year_of_production, item.description))
        return items

    def getOrderbyCustomer(self, customer_order_id):
        orders = []
        for order in self.session.query(Order).order_by(Order.id).filter(Order.customer_order_id == customer_order_id):
            orders.append((order.id, order.customer_order_id, order.item_id, order.price))
        return orders

    def getOrderbyItem(self, item_id):
        orders = []
        for order in self.session.query(Order).order_by(Order.id).filter(Order.item_id == item_id):
            orders.append((order.id, order.customer_order_id, order.item_id, order.price))
        return orders

    def deleteItembyInstrumentId(self, instrument_id):
        self.session.execute(delete(Item).where(Item.instrument_id == instrument_id))
        self.session.commit()

    def deleteOrderbyCustomer(self, customer_order_id):
        self.session.execute(delete(Order).where(Order.customer_order_id == customer_order_id))
        self.session.commit()

    def deleteOrderbyItem(self, item_id):
        self.session.execute(delete(Order).where(Order.item_id == item_id))
        self.session.commit()

    def generateInstruments(self, quantity):
        self.cursor.execute(f"""INSERT INTO instrument (name, country, category) select 
        chr(trunc(65 + random()*57)::int) || chr(trunc(65 + random()*57)::int) || chr(trunc(65 + random()*57)::int) || 
        chr(trunc(65 + random()*57)::int), chr(trunc(65 + random()*57)::int) || chr(trunc(65 + random()*57)::int) || 
        chr(trunc(65 + random()*57)::int) || chr(trunc(65 + random()*57)::int), chr(trunc(65 + random()*57)::int) || 
        chr(trunc(65 + random()*57)::int) || chr(trunc(65 + random()*57)::int) || chr(trunc(65 + random()*57)::int) 
        from generate_series(1, {quantity})""")
        self.con.commit()

    def generateItems(self,quantity):
        self.cursor.execute(f"""INSERT INTO item (instrument_id, year_of_production, description) select 
        get_instrument_id(), floor(random() * (2020-1950+1) + 1980)::int,
        chr(trunc(65 + random()*57)::int) || chr(trunc(65 + random()*57)::int) || chr(trunc(65 + random()*57)::int) || 
        chr(trunc(65 + random()*57)::int)
        from generate_series(1, {quantity})""")
        self.con.commit()

    def generateCustomerOrder(self, quantity):
        self.cursor.execute(f"""INSERT INTO customer_order (customer_name, status, time) select 
        chr(trunc(65 + random()*57)::int) || chr(trunc(65 + random()*57)::int) || chr(trunc(65 + random()*57)::int) || 
        chr(trunc(65 + random()*57)::int),
        False,
        timestamp '2010-01-10 20:00:00' + random() * (timestamp '2010-01-20 20:00:00' - timestamp '2020-12-20 10:00:00')
        from generate_series(1, {quantity})""")
        self.con.commit()

    def generateOrderItem(self, quantity):
        self.cursor.execute(f"""INSERT INTO order_item (customer_order_id, item_id, price) select 
        get_customer_order_id(),
        get_item_id(), floor(random() *(5000-1+1) + 1)::int
        from generate_series(1, {quantity})""")
        self.con.commit()

    def getCustomerOrders(self, customer_name=""):
        self.cursor.execute(f"""with main as(SELECT item_id FROM order_item 
        INNER JOIN (SELECT id FROM customer_order WHERE customer_name='{customer_name}') AS c ON 
        order_item.customer_order_id=c.id)
        SELECT id, name, country, category FROM instrument
        INNER JOIN main 
        ON instrument.id=main.item_id
        ORDER BY id""")
        rows = self.cursor.fetchall()
        return rows

    def getInstrumentByCountryOrCategoty(self, country="", category=""):
        self.cursor.execute(f""" with main as (SELECT * FROM instrument 
        WHERE  category  like '%{category}%' OR country like '%{country}%')
        select main.name, main.category, main.country, item.year_of_production, item.description
        from main inner join item 
        on item.instrument_id=main.id""")
        rows = self.cursor.fetchall()
        return rows

    def getItemByPrice(self, price=100):
        self.cursor.execute(f"""with main as(select item_id, price from order_item where price<{price})
        select name, country, main2.price from instrument 
        inner join (select item.instrument_id, main.price from item inner join main on item.id=main.item_id) as main2
        on instrument.id=main2.instrument_id""")
        rows = self.cursor.fetchall()
        return rows