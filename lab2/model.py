import psycopg2
import string
from datetime import date
import random
from sqlalchemy import create_engine, update, delete
from sqlalchemy.orm import sessionmaker

class Model(object):
    def __init__(self, dbname_='postgres', user_='postgres', password_='12345687', host_='localhost'):
        self.con = psycopg2.connect(dbname=f'{dbname_}', user=f'{user_}', password=f'{password_}', host=f'{host_}')
        self.cursor = self.con.cursor()



    def __del__(self):
        self.con.close()

    def getInstrument(self):
        self.cursor.execute('SELECT * FROM instrument ORDER BY id')
        rows = self.cursor.fetchall()
        return rows

    def getInstrumentbyId(self, id):
        self.cursor.execute(f'SELECT * FROM instrument WHERE id={id}')
        item = self.cursor.fetchall()
        return item

    def addInstrument(self, name, country, category):
        self.cursor.execute(f"INSERT INTO instrument (name, country, category) VALUES ('{name}', '{country}', '{category}')")
        self.con.commit()

    def updateInstrument(self, id, name, country, category):
        self.cursor.execute(f"UPDATE instrument SET name = '{name}', country = '{country}', category = '{category}' WHERE id ={id} ")
        self.con.commit()

    def deleteInstrument(self, id):
        self.cursor.execute(f"DELETE FROM instrument WHERE id = {id}")
        self.con.commit()

    ###################
    def getCustomerOrder(self):
        self.cursor.execute('SELECT * FROM customer_order ORDER BY id')
        rows = self.cursor.fetchall()
        return rows

    def getCustomerOrderbyId(self, id):
        self.cursor.execute(f'SELECT * FROM customer_order WHERE id={id}')
        item = self.cursor.fetchall()
        return item

    def addCustomerOrder(self, customer_name, status, time):
        self.cursor.execute(f"INSERT INTO customer_order (customer_name, status, time) VALUES ('{customer_name}', '{status}', '{time}')")
        self.con.commit()

    def updateCustomerOrder(self, id, customer_name, status, time):
        self.cursor.execute(f"UPDATE customer_order SET customer_name = '{customer_name}', status = '{status}', time = '{time}' WHERE id ={id} ")
        self.con.commit()

    def deleteCustomerOrder(self, id):
        self.cursor.execute(f"DELETE FROM customer_order WHERE id = {id}")
        self.con.commit()


###################
    def getItem(self):
        self.cursor.execute('SELECT * FROM item ORDER BY id')
        rows = self.cursor.fetchall()
        return rows


    def getItembyId(self, id):
        self.cursor.execute(f'SELECT * FROM item WHERE id={id}')
        item = self.cursor.fetchall()
        return item


    def addItem(self, instrument_id, year_of_production, description):
        self.cursor.execute(f"INSERT INTO item (instrument_id, year_of_production, description) VALUES ('{instrument_id}', '{year_of_production}', '{description}')")
        self.con.commit()


    def updateItem(self, id, instrument_id, year_of_production, description):
        self.cursor.execute(
            f"UPDATE item SET instrument_id = '{instrument_id}', year_of_production = '{year_of_production}', description = '{description}' WHERE id ={id} ")
        self.con.commit()


    def deleteItem(self, id):
        self.cursor.execute(f"DELETE FROM item WHERE id = {id}")
        self.con.commit()
    ###################

    def getOrder(self):
        self.cursor.execute('SELECT * FROM order_item ORDER BY id')
        rows = self.cursor.fetchall()
        return rows


    def getOrderbyId(self, id):
        self.cursor.execute(f'SELECT * FROM order_item WHERE id={id}')
        item = self.cursor.fetchall()
        return item


    def addOrder(self, customer_order_id, item_id, price):
        self.cursor.execute(
            f"INSERT INTO order_item (customer_order_id, item_id, price) VALUES ('{customer_order_id}', '{item_id}', '{price}')")
        self.con.commit()


    def updateOrder(self, id, customer_order_id, item_id, price):
        self.cursor.execute(
            f"UPDATE order_item SET customer_order_id = '{customer_order_id}', item_id = '{item_id}', price = '{price}' WHERE id ={id} ")
        self.con.commit()


    def deleteOrder(self, id):
        self.cursor.execute(f"DELETE FROM order_item WHERE id = {id}")
        self.cursor.execute(f"UPDATE customer_order SET status = 'False' WHERE id={id} ")
        self.con.commit()

    def getItembyInstrumentId(self, instrument_id):
        self.cursor.execute(f'SELECT * FROM item WHERE instrument_id={instrument_id}  ORDER BY id')
        rows = self.cursor.fetchall()
        return rows

    def getOrderbyCustomer(self, customer_order_id):
        self.cursor.execute(f'SELECT * FROM order_item WHERE customer_order_id={customer_order_id}  ORDER BY id')
        rows = self.cursor.fetchall()
        return rows


    def getOrderbyItem(self, item_id):
        self.cursor.execute(f'SELECT * FROM order_item WHERE item_id={item_id} ORDER BY id')
        item = self.cursor.fetchall()
        return item

    def deleteItembyInstrumentId(self, instrument_id):
        self.cursor.execute(f"DELETE FROM item WHERE instrument_id = {instrument_id}")
        self.con.commit()

    def deleteOrderbyCustomer(self, customer_order_id):
        self.cursor.execute(f"DELETE FROM order_item WHERE customer_order_id = {customer_order_id}")
        self.con.commit()

    def deleteOrderbyItem(self, item_id):
        self.cursor.execute(f"DELETE FROM order_item WHERE item_id = {item_id}")
        self.con.commit()

    def generateInstruments(self, quantity):
        self.cursor.execute(f"""INSERT INTO instrument (name, country, category) select 
        chr(trunc(65 + random()*57)::int) || chr(trunc(65 + random()*57)::int) || chr(trunc(65 + random()*57)::int) || 
        chr(trunc(65 + random()*57)::int), chr(trunc(65 + random()*57)::int) || chr(trunc(65 + random()*57)::int) || 
        chr(trunc(65 + random()*57)::int) || chr(trunc(65 + random()*57)::int), chr(trunc(65 + random()*57)::int) || 
        chr(trunc(65 + random()*57)::int) || chr(trunc(65 + random()*57)::int) || chr(trunc(65 + random()*57)::int) 
        from generate_series(1, {quantity})""")
        self.con.commit()

    def generateItems(self,quantity):
        self.cursor.execute(f"""INSERT INTO item (instrument_id, year_of_production, description) select 
        get_instrument_id(), floor(random() * (2020-1950+1) + 1980)::int,
        chr(trunc(65 + random()*57)::int) || chr(trunc(65 + random()*57)::int) || chr(trunc(65 + random()*57)::int) || 
        chr(trunc(65 + random()*57)::int)
        from generate_series(1, {quantity})""")
        self.con.commit()

    def generateCustomerOrder(self, quantity):
        self.cursor.execute(f"""INSERT INTO customer_order (customer_name, status, time) select 
        chr(trunc(65 + random()*57)::int) || chr(trunc(65 + random()*57)::int) || chr(trunc(65 + random()*57)::int) || 
        chr(trunc(65 + random()*57)::int),
        False,
        timestamp '2010-01-10 20:00:00' + random() * (timestamp '2010-01-20 20:00:00' - timestamp '2020-12-20 10:00:00')
        from generate_series(1, {quantity})""")
        self.con.commit()

    def generateOrderItem(self, quantity):
        self.cursor.execute(f"""INSERT INTO order_item (customer_order_id, item_id, price) select 
        get_customer_order_id(),
        get_item_id(), floor(random() *(5000-1+1) + 1)::int
        from generate_series(1, {quantity})""")
        self.con.commit()

    def getCustomerOrders(self, customer_name=""):
        self.cursor.execute(f"""with main as(SELECT item_id FROM order_item 
        INNER JOIN (SELECT id FROM customer_order WHERE customer_name='{customer_name}') AS c ON 
        order_item.customer_order_id=c.id)
        SELECT id, name, country, category FROM instrument
        INNER JOIN main 
        ON instrument.id=main.item_id
        ORDER BY id""")
        rows = self.cursor.fetchall()
        return rows

    def getInstrumentByCountryOrCategoty(self, country="", category=""):
        self.cursor.execute(f""" with main as (SELECT * FROM instrument 
        WHERE  category  like '%{category}%' OR country like '%{country}%')
        select main.name, main.category, main.country, item.year_of_production, item.description
        from main inner join item 
        on item.instrument_id=main.id""")
        rows = self.cursor.fetchall()
        return rows

    def getItemByPrice(self, price=100):
        self.cursor.execute(f"""with main as(select item_id, price from order_item where price<{price})
        select name, country, main2.price from instrument 
        inner join (select item.instrument_id, main.price from item inner join main on item.id=main.item_id) as main2
        on instrument.id=main2.instrument_id""")
        rows = self.cursor.fetchall()
        return rows