from texttable import Texttable
import string

class View(object):

    def __init__(self):
        self.instruments = Texttable()
        self.item = Texttable()
        self.customer_order = Texttable()
        self.order_item = Texttable()
        self.joiners = Texttable()

    def MainMenu(self):
        print("1. Work with the customer's orders")
        print("2. Work with the instruments")
        print("3. Work with the items")
        print("4. Work with the orders")
        print("5. Search by customer name")
        print("6. Search by country or category of the instrument")
        print("7. Search by price")
        print("8. End")

    def customerMenu(self):
        print("1. Get customer's orders")
        print("2. Get one by id")
        print("3. Add a new one")
        print("4. Update an old one")
        print("5. Delete an order")
        print("6. Generate random customer's orders")
        print("7. Back to the main menu")

    def instrumentMenu(self):
        print("1. Get all the instruments")
        print("2. Get one by id")
        print("3. Add a new one")
        print("4. Update an old one")
        print("5. Delete an instrument")
        print("6. Generate random instruments")
        print("7. Back to the main menu")

    def itemMenu(self):
        print("1. Get all the items")
        print("2. Get one by id")
        print("3. Add a new one")
        print("4. Update an old one")
        print("5. Delete an item")
        print("6. Generate random items")
        print("7. Back to the main menu")

    def orderMenu(self):
        print("1. Get all the orders")
        print("2. Get one by id")
        print("3. Add a new one")
        print("4. Update an old one")
        print("5. Delete an order")
        print("6. Generate random orders")
        print("7. Back to the main menu")

    def selectedNumber(self):
        return input("Choose the option from 1 to 8: ")

    def inputId(self):
        return input("Enter id: ")
    def inputIdofInstrument(self):
        return input("Enter id of instrument: ")
    def inputIdOfOrder(self):
        return input("Enter id of your order: ")
    def inputIdOfItem(self):
        return input("Enter id of item: ")
    def noId(self):
        print("No such id")
    def noNum(self, tmp):
        print(f"{tmp} must be a number & more than 0")
    def inputName(self):
        return input("Enter name: ")
    def inputStatus(self):
        return input("Enter the status of your order: ")
    def inputTime(self):
        return input("Enter date and time of the order: ")
    def inputCountry(self):
        return input("Enter the country, where instrument  came from: : ")
    def inputCategory(self):
        return input("Enter the category of instrument: ")
    def newError(self):
        print("You have entered wrong input..")
    def inputQuantity(self):
        return input("Enter the quantity for randomazing: ")
    def inputYear(self):
        return input("Enter the year of production of the instrument: ")
    def inputDescription(self):
        return input("Enter some description: ")
    def inputPrice(self):
        return input("Enter new price: ")

    def printInstrument(self, row):
        self.instruments.reset()
        row = [("Instrument id", "Instrument name", "Country of instrument", "Instrument category")] + row
        self.instruments.add_rows(row)
        print(self.instruments.draw())

    def printCustomerOrder(self, row):
        self.customer_order.reset()
        row = [("id", "name", "status", "time")] + row
        self.customer_order.add_rows(row)
        print(self.customer_order.draw())

    def printItem(self, row):
        self.item.reset()
        row = [("id", "instrument id","year of production", "description")] + row
        self.customer_order.add_rows(row)
        print(self.customer_order.draw())

    def printOrder(self, row):
        self.order_item.reset()
        row = [("id", "customer order id", "item id", "price")] + row
        self.order_item.add_rows(row)
        print(self.order_item.draw())

    def printDefTime(self, start_time, end_time):
        time = (end_time - start_time)*1000
        print("Time spent: "+ str(time) + " ms")

    def printSearch1(self, row):
        self.joiners.reset()
        row = [("id","name", "country", "category")] + row
        self.joiners.add_rows(row)
        print(self.joiners.draw())

    def printSearch2(self, row):
        self.joiners.reset()
        row = [("name", "category", "country", "year", "description")] + row
        self.joiners.add_rows(row)
        print(self.joiners.draw())

    def printSearch3(self, row):
        self.joiners.reset()
        row = [("name", "country", "price")] + row
        self.joiners.add_rows(row)
        print(self.joiners.draw())

    def printExit(self):
        print("Bye-bye:)")