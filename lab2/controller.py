from model import Model
from view import View
from datetime import datetime
from time import time

class Controller:

    def __init__(self):
        self.view = View()
        self.model = Model()
        self.mainMenu()

    # Main Menu Check

    def mainMenu(self):
        while True:
            self.view.MainMenu()
            number = self.view.selectedNumber()
            if number.isdigit():
                switcher = {
                    1: self.customersMenu,
                    2: self.instrumentsMenu,
                    3: self.itemsMenu,
                    4: self.ordersMenu,
                    5: self.search1,
                    6: self.search2,
                    7: self.search3,
                    8: self.exit
                }
                switcher.get(int(number), lambda: print("Incorrect input number"))()
            if int(number) == 8:
                break

    def exit(self):
        self.view.printExit()

    def customersMenu(self):
        self.view.customerMenu()
        number=self.view.selectedNumber()
        if number.isdigit():
            switcher={
                1: self.CustomerOrderDisplay,
                2: self.CustomerOrderGet,
                3: self.CustomerOrderCreate,
                4: self.CustomerOrderModify,
                5: self.CustomerOrderRemove,
                6: self.CustomerOrderRandomize,
                7: self.mainMenu
            }
            switcher.get(int(number), lambda: print("Incorrect input number"))()

    def instrumentsMenu(self):
        self.view.instrumentMenu()
        number=self.view.selectedNumber()
        if number.isdigit():
            switcher={
                1: self.InstrumentDisplay,
                2: self.InstrumentGet,
                3: self.InstrumentCreate,
                4: self.InstrumentModify,
                5: self.InstrumentRemove,
                6: self.InstrumentRandomize,
                7: self.mainMenu
            }
            switcher.get(int(number), lambda: print("Incorrect input number"))()

    def itemsMenu(self):
        self.view.itemMenu()
        number=self.view.selectedNumber()
        if number.isdigit():
            switcher={
                1: self.ItemDisplay,
                2: self.ItemGet,
                3: self.ItemCreate,
                4: self.ItemModify,
                5: self.ItemRemove,
                6: self.ItemRandomize,
                7: self.mainMenu
            }
            switcher.get(int(number), lambda: print("Incorrect input number"))()

    def ordersMenu(self):
        self.view.orderMenu()
        number=self.view.selectedNumber()
        if number.isdigit():
            switcher={
                1: self.OrderDisplay,
                2: self.OrderGet,
                3: self.OrderCreate,
                4: self.OrderModify,
                5: self.OrderRemove,
                6: self.OrderRandomize,
                7: self.mainMenu
            }
            switcher.get(int(number), lambda: print("Incorrect input number"))()

    def search1(self):
        name = self.view.inputName()
        if int(len(name)) > 0:
            start_time = time()
            self.view.printSearch1(self.model.getCustomerOrders(name))
            end_time = time()
            self.view.printDefTime(start_time, end_time)
        else:
            self.view.newError()
    def search2(self):
        country = self.view.inputCountry()
        category=self.view.inputCategory()
        if int(len(country)) > 0 or int(len(category)) > 0:
            start_time = time()
            self.view.printSearch2(self.model.getInstrumentByCountryOrCategoty(country, category))
            end_time = time()
            self.view.printDefTime(start_time, end_time)
        else: self.view.newError()

    def search3(self):
        price = self.view.inputPrice()
        if price.isdigit() and int(price) > 0:
            start_time = time()
            self.view.printSearch3(self.model.getItemByPrice(price))
            end_time = time()
            self.view.printDefTime(start_time, end_time)
        else:
            self.view.newError()

    #Customer order
    def CustomerOrderDisplay(self):
        self.view.printCustomerOrder(self.model.getCustomerOrder())

    def CustomerOrderGet(self):
        id_=self.view.inputId()
        if id_.isdigit():
            try:
                main=self.model.getCustomerOrderbyId(int(id_))
                self.view.printCustomerOrder(main)
            except: self.view.noId()
        else: self.view.noNum("id")

    def CustomerOrderCreate(self):
        name=self.view.inputName()
        status=self.view.inputStatus()
        time=self.view.inputTime()
        if datetime.strptime(time, '%Y-%m-%d %H:%M:%S') and (status or not status) and int(len(name))>0:
            self.model.addCustomerOrder(name, status, time)
        else: self.view.newError()

    def CustomerOrderModify(self):
        id_=self.view.inputId()
        name = self.view.inputName()
        status = self.view.inputStatus()
        time = self.view.inputTime()
        if datetime.strptime(time, '%Y-%m-%d %H:%M:%S') and (status or not status) and int(len(name)) > 0:
            try:
                self.model.updateCustomerOrder(id_, name, status, time)
                row=self.model.getCustomerOrderbyId(id_)
                self.view.printCustomerOrder(row)
            except:
                self.view.noId()
        else: self.view.newError()

    def CustomerOrderRemove(self):
        id_=self.view.inputId()
        try:
            row = self.model.getCustomerOrderbyId(id_)
            self.view.printCustomerOrder(row)
            self.model.getOrderbyCustomer(id_)
            self.model.deleteOrderbyCustomer(id_)
            self.model.deleteCustomerOrder(id_)
        except:
            self.view.noId()

    def CustomerOrderRandomize(self):
        quantity=self.view.inputQuantity()
        if quantity.isnumeric():
            self.model.generateCustomerOrder(quantity)
            self.CustomerOrderDisplay()
        else:
            self.view.noNum(quantity)

#instrument
    def InstrumentDisplay(self):
        self.view.printInstrument(self.model.getInstrument())

    def InstrumentGet(self):
        id_=self.view.inputId()
        if id_.isdigit():
            try: self.view.printInstrument(self.model.getInstrumentbyId(int(id_)))
            except: self.view.noId()
        else: self.view.noNum("id")

    def InstrumentCreate(self):
        name=self.view.inputName()
        country=self.view.inputCountry()
        category=self.view.inputCategory()
        if int(len(name))>0 and int(len(country))>0 and int(len(category))>0 :
            self.model.addInstrument(name, country, category)
        else: self.view.newError()

    def InstrumentModify(self):
        id_=self.view.inputId()
        name = self.view.inputName()
        country = self.view.inputCountry()
        category = self.view.inputCategory()
        if int(len(name))>0 and int(len(country))>0 and int(len(category))>0:
            try:
                self.model.getInstrumentbyId(id_)
                self.model.updateInstrument(id_, name, country, category)
                row=self.model.getInstrumentbyId(id_)
                self.view.printInstrument(row)
            except:
                self.view.noId()
        else: self.view.newError()

    def InstrumentRemove(self):
        id_=self.view.inputId()
        try:
            row = self.model.getInstrumentbyId(id_)
            self.view.printInstrument(row)
            self.model.getItembyInstrumentId(id_)
            self.model.deleteItembyInstrumentId(id_)
            self.model.deleteInstrument(id_)
        except:
            self.view.noId()

    def InstrumentRandomize(self):
        quantity=self.view.inputQuantity()
        if quantity.isnumeric():
            self.model.generateInstruments(quantity)
            self.InstrumentDisplay()
        else:
            self.view.noNum(quantity)

#item
    def ItemDisplay(self):
        self.view.printItem(self.model.getItem())

    def ItemGet(self):
        id_=self.view.inputId()
        if id_.isdigit():
            try:
                self.view.printItem(self.model.getItembyId(int(id_)))
            except:
                self.view.noId()
        else: self.view.noNum("id")

    def ItemCreate(self):
        instrument_id=self.view.inputIdofInstrument()
        year_of_production=self.view.inputYear()
        description=self.view.inputDescription()
        if instrument_id.isdigit() and year_of_production.isdigit() and 1900 < int(year_of_production) < 2021 and int(len(description))>0 :
            try:
                self.model.getInstrumentbyId(instrument_id)
                self.model.addItem(instrument_id, year_of_production, description)
            except:
                self.view.noId()
        else: self.view.newError()

    def ItemModify(self):
        id_=self.view.inputId()
        instrument_id = self.view.inputIdofInstrument()
        year_of_production = self.view.inputYear()
        description = self.view.inputDescription()
        if instrument_id.isdigit() and year_of_production.isdigit() and 1900 < int(year_of_production) < 2021 and int(len(description))>0 :
            try:
                self.model.getItembyInstrumentId(instrument_id)
                self.model.updateItem(id_, instrument_id, year_of_production, description)
                row=self.model.getItembyId(id_)
                self.view.printItem(row)
            except:
                self.view.noId()
        else: self.view.newError()

    def ItemRemove(self):
        id_=self.view.inputId()
        try:
            row = self.model.getItembyId(id_)
            self.view.printItem(row)
            self.model.getOrderbyItem(id_)
            self.model.deleteOrderbyItem(id_)
            self.model.deleteItem(id_)
        except:
            self.view.noId()

    def ItemRandomize(self):
        quantity=self.view.inputQuantity()
        if quantity.isnumeric():
            self.model.generateItems(quantity)
            self.ItemDisplay()
        else:
            self.view.noNum(quantity)

#order
    def OrderDisplay(self):
        self.view.printOrder(self.model.getOrder())

    def OrderGet(self):
        id_=self.view.inputId()
        if id_.isdigit():
            try:

                self.view.printOrder(self.model.getOrderbyId(int(id_)))
            except:
                self.view.noId()
        else: self.view.noNum("id")

    def OrderCreate(self):
        customer_order_id=self.view.inputIdOfOrder()
        item_id=self.view.inputIdOfItem()
        price=self.view.inputPrice()
        if customer_order_id.isdigit() and item_id.isdigit() and price.isdigit() and int(price)>0:
            try:
                self.model.getItembyId(item_id)
                self.model.getCustomerOrderbyId(customer_order_id)
                self.model.addOrder(customer_order_id, item_id, price)
            except:
                self.view.noId()
        else: self.view.newError()

    def OrderModify(self):
        id_=self.view.inputId()
        customer_order_id = self.view.inputIdOfOrder()
        item_id = self.view.inputIdOfItem()
        price = self.view.inputPrice()
        if customer_order_id.isdigit() and item_id.isdigit() and price.isdigit() and int(price) > 0:
            try:
                self.model.getItembyId(item_id)
                self.model.getCustomerOrderbyId(customer_order_id)
                self.model.updateOrder(id_, customer_order_id, item_id, price)
                row=self.model.getOrderbyId(id_)
                self.view.printOrder(row)
            except:
                self.view.noId()
        else: self.view.newError()

    def OrderRemove(self):
        id_=self.view.inputId()
        try:
            row = self.model.getOrderbyId(id_)
            self.view.printOrder(row)
            self.model.deleteOrder(id_)
        except:
            self.view.noId()

    def OrderRandomize(self):
        quantity=self.view.inputQuantity()
        if quantity.isnumeric():
            self.model.generateOrderItem(quantity)
            self.OrderDisplay()
        else: self.view.noNum(quantity)