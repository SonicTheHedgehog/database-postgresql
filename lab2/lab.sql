PGDMP         $                x            postgres #   12.5 (Ubuntu 12.5-0ubuntu0.20.04.1)     13.0 (Ubuntu 13.0-1.pgdg18.04+1)     �           0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                      false            �           0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                      false            �           0    0 
   SEARCHPATH 
   SEARCHPATH     8   SELECT pg_catalog.set_config('search_path', '', false);
                      false            �           1262    13438    postgres    DATABASE     ]   CREATE DATABASE postgres WITH TEMPLATE = template0 ENCODING = 'UTF8' LOCALE = 'uk_UA.UTF-8';
    DROP DATABASE postgres;
                postgres    false            �           0    0    DATABASE postgres    COMMENT     N   COMMENT ON DATABASE postgres IS 'default administrative connection database';
                   postgres    false    2981            �            1255    16848    get_customer_order_id()    FUNCTION     �   CREATE FUNCTION public.get_customer_order_id() RETURNS integer
    LANGUAGE plpgsql
    AS $$
DECLARE
    new_int int;
BEGIN
    SELECT id INTO new_int FROM customer_order ORDER BY RANDOM() LIMIT 1;
    RETURN new_int;
END;
$$;
 .   DROP FUNCTION public.get_customer_order_id();
       public          postgres    false            �            1255    16846    get_instrument_id()    FUNCTION     �   CREATE FUNCTION public.get_instrument_id() RETURNS integer
    LANGUAGE plpgsql
    AS $$
DECLARE
    new_int int;
BEGIN
    SELECT id INTO new_int FROM instrument ORDER BY RANDOM() LIMIT 1;
    RETURN new_int;
END;
$$;
 *   DROP FUNCTION public.get_instrument_id();
       public          postgres    false            �            1255    16847    get_item_id()    FUNCTION     �   CREATE FUNCTION public.get_item_id() RETURNS integer
    LANGUAGE plpgsql
    AS $$
DECLARE
    new_int int;
BEGIN
    SELECT id INTO new_int FROM item ORDER BY RANDOM() LIMIT 1;
    RETURN new_int;
END;
$$;
 $   DROP FUNCTION public.get_item_id();
       public          postgres    false            �            1259    16767    customer_order    TABLE     �   CREATE TABLE public.customer_order (
    id integer NOT NULL,
    customer_name character varying NOT NULL,
    status boolean,
    "time" timestamp without time zone
);
 "   DROP TABLE public.customer_order;
       public         heap    postgres    false            �            1259    16840    customer_order_id_seq    SEQUENCE     �   ALTER TABLE public.customer_order ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.customer_order_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);
            public          postgres    false    204            �            1259    16746 
   instrument    TABLE     �   CREATE TABLE public.instrument (
    id integer NOT NULL,
    name character varying NOT NULL,
    country character varying NOT NULL,
    category character varying NOT NULL
);
    DROP TABLE public.instrument;
       public         heap    postgres    false            �            1259    16838    instrument_id_seq    SEQUENCE     �   ALTER TABLE public.instrument ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.instrument_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);
            public          postgres    false    202            �            1259    16754    item    TABLE     �   CREATE TABLE public.item (
    id integer NOT NULL,
    instrument_id integer NOT NULL,
    year_of_production integer NOT NULL,
    description character varying
);
    DROP TABLE public.item;
       public         heap    postgres    false            �            1259    16842    item_id_seq    SEQUENCE     �   ALTER TABLE public.item ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.item_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);
            public          postgres    false    203            �            1259    16775 
   order_item    TABLE     �   CREATE TABLE public.order_item (
    id integer NOT NULL,
    customer_order_id integer NOT NULL,
    item_id integer NOT NULL,
    price integer NOT NULL
);
    DROP TABLE public.order_item;
       public         heap    postgres    false            �            1259    16844    order_item_id_seq    SEQUENCE     �   ALTER TABLE public.order_item ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.order_item_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);
            public          postgres    false    205            �          0    16767    customer_order 
   TABLE DATA           K   COPY public.customer_order (id, customer_name, status, "time") FROM stdin;
    public          postgres    false    204   �#       �          0    16746 
   instrument 
   TABLE DATA           A   COPY public.instrument (id, name, country, category) FROM stdin;
    public          postgres    false    202   �$       �          0    16754    item 
   TABLE DATA           R   COPY public.item (id, instrument_id, year_of_production, description) FROM stdin;
    public          postgres    false    203   h%       �          0    16775 
   order_item 
   TABLE DATA           K   COPY public.order_item (id, customer_order_id, item_id, price) FROM stdin;
    public          postgres    false    205   �%       �           0    0    customer_order_id_seq    SEQUENCE SET     D   SELECT pg_catalog.setval('public.customer_order_id_seq', 15, true);
          public          postgres    false    207            �           0    0    instrument_id_seq    SEQUENCE SET     @   SELECT pg_catalog.setval('public.instrument_id_seq', 16, true);
          public          postgres    false    206            �           0    0    item_id_seq    SEQUENCE SET     :   SELECT pg_catalog.setval('public.item_id_seq', 12, true);
          public          postgres    false    208            �           0    0    order_item_id_seq    SEQUENCE SET     @   SELECT pg_catalog.setval('public.order_item_id_seq', 19, true);
          public          postgres    false    209                       2606    16774 "   customer_order Customer_order_pkey 
   CONSTRAINT     b   ALTER TABLE ONLY public.customer_order
    ADD CONSTRAINT "Customer_order_pkey" PRIMARY KEY (id);
 N   ALTER TABLE ONLY public.customer_order DROP CONSTRAINT "Customer_order_pkey";
       public            postgres    false    204                       2606    16753    instrument Instrument_pkey 
   CONSTRAINT     Z   ALTER TABLE ONLY public.instrument
    ADD CONSTRAINT "Instrument_pkey" PRIMARY KEY (id);
 F   ALTER TABLE ONLY public.instrument DROP CONSTRAINT "Instrument_pkey";
       public            postgres    false    202                       2606    16761    item Item_pkey 
   CONSTRAINT     N   ALTER TABLE ONLY public.item
    ADD CONSTRAINT "Item_pkey" PRIMARY KEY (id);
 :   ALTER TABLE ONLY public.item DROP CONSTRAINT "Item_pkey";
       public            postgres    false    203                       2606    16779    order_item order_item_pkey 
   CONSTRAINT     X   ALTER TABLE ONLY public.order_item
    ADD CONSTRAINT order_item_pkey PRIMARY KEY (id);
 D   ALTER TABLE ONLY public.order_item DROP CONSTRAINT order_item_pkey;
       public            postgres    false    205                       2606    16785    order_item customer_order_id    FK CONSTRAINT     �   ALTER TABLE ONLY public.order_item
    ADD CONSTRAINT customer_order_id FOREIGN KEY (customer_order_id) REFERENCES public.customer_order(id) NOT VALID;
 F   ALTER TABLE ONLY public.order_item DROP CONSTRAINT customer_order_id;
       public          postgres    false    2836    204    205                       2606    16762    item instrument_id    FK CONSTRAINT     |   ALTER TABLE ONLY public.item
    ADD CONSTRAINT instrument_id FOREIGN KEY (instrument_id) REFERENCES public.instrument(id);
 <   ALTER TABLE ONLY public.item DROP CONSTRAINT instrument_id;
       public          postgres    false    2832    203    202                       2606    16790    order_item item_id    FK CONSTRAINT     z   ALTER TABLE ONLY public.order_item
    ADD CONSTRAINT item_id FOREIGN KEY (item_id) REFERENCES public.item(id) NOT VALID;
 <   ALTER TABLE ONLY public.order_item DROP CONSTRAINT item_id;
       public          postgres    false    205    2834    203            �   �   x�e��JC1���S�H�ߤ�RE�����
j���Q�o��^)����q�!g_��BB�9I�t�n��֢�
��H��{�y����{
ܬ����_)��dX��f<BLS��a����˔O��&����k�fƥ 3<]=_�(�&�j�n-j�GX���I�tZj1Gs�&�!+|�w?3��.��v�-��,H,/>f]-�B]k���y���� i?H�      �   �   x�M��N�0��>O���I�S�TT�*P���v�w�NKޞ
Q��hf�ڤ.$���X��UbuG�b�'��w�	ǬA��,	e�8���`#���M��sG�B�A����:���}�Xx=�\ܔ����d������K�����A���nGf��&l��\�xV��L0o�����>�L�x.v�ۗ��V�zKD?sDN�      �      x���
1�ݧ�	$_å�?��� 6wI�I���ޜ�ˌ@C+Q8ԚV�djX���P�˛�vQ
�kIǾ$Z��i��a~�yb߈v��E�m��s���ݒq�b�S�4�����cM���R      �   C   x����0�0Leb���t�9��{pB� 4\�̂���"���r�P9'݁�MmL����X|/��'     